FROM python:3.7-alpine AS build

RUN apk --no-cache add gcc musl-dev linux-headers openssl-dev
RUN python -m venv /app/venv && /app/venv/bin/pip install --upgrade pip

WORKDIR /app
COPY requirements.txt /app
RUN /app/venv/bin/pip install -r requirements.txt

FROM python:3.7-alpine
COPY --from=build /app/venv /app/venv
ENV PATH="/app/venv/bin:${PATH}"

COPY . /app
WORKDIR /app

EXPOSE 5000
USER nobody
CMD ["uwsgi", "--http", ":5000", "--manage-script-name", "--mount", "/=app:app"]
