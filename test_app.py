import pytest
from app import app


def test_hallo_wereld():
    with app.test_client() as c:
        response = c.get('/')

    assert ('Hallo wereld!' in str(response.data))
