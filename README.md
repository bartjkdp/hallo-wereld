# Hallo wereld
Dit is een webapplicatie met een bijgevoegde Dockerfile en yaml om uit te rollen op Kubernetes.

## De applicatie bouwen

```bash
docker build -t hallo-wereld .
```

## De applicatie draaien

```bash
docker run -p 5000:5000 hallo-wereld
```

Je kunt de applicatie nu bekijken op [http://localhost:5000](http://localhost:5000).

## Uitrollen naar Kubernetes

Gebruik het volgende commando om versie 1.0 uit te rollen:

```bash
kubectl apply -f v1.yaml
```

En het volgende commando om versie 2.0 uit te rollen:

```bash
kubectl apply -f v2.yaml
```

Gebruik het volgende commando om de applicatie te verwijderen:

```bash
kubectl delete -f v1.yaml
```

## Handige commando's

De Kubernetes pods bekijken:

```bash
watch -n 1 kubectl get pods -o wide
```

De applicatie schalen:

```bash
kubectl scale deployment hallo-wereld --replicas=3
```

Oneindig de website http://hallo-wereld.minikube/ opvragen:

```bash
while sleep 1; do curl http://hallo-wereld.minikube/; done
```