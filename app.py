from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hallo wereld! (en Den Haag). Ik ben applicatie versie 2.0\n"


if __name__ == "__main__":
    app.run(debug=True)  # Wordt niet uitgevoerd in uwsgi mode
